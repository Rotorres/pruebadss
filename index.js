const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const { body, validationResult } = require('express-validator');
const expressSanitizer = require('express-sanitizer');
const app = express();
const PORT = process.env.PORT || 3000;

// Configurar Express para usar body-parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressSanitizer()); 

// Configurar Express para servir archivos estáticos desde la carpeta 'public'
app.use(express.static(path.join(__dirname, 'public')));

// Ruta para servir el formulario HTML de ingreso de datos
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'public', 'ingreso_datos.html'));
});

// Validación y saneamiento de los datos del formulario
app.post('/datos', 
    body('año').trim().escape(),
    body('marca').trim().escape(),
    body('color').trim().escape(),
    body('combustible').trim().escape(),
    body('num_puertas').trim().escape(),
    body('traccion').trim().escape(),
    (req, res) => {
        // Obtener los errores de validación
        const errors = validationResult(req);
        
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }
        
        // Recuperar los datos enviados desde el formulario
        const { año, marca, color, combustible, num_puertas, traccion } = req.body;

        // Enviar los datos como objeto JSON al cliente
        res.json({ año, marca, color, combustible, num_puertas, traccion });
    }
);

// Iniciar el servidor
app.listen(PORT, () => {
    console.log(`Servidor escuchando en el puerto ${PORT}`);
});
